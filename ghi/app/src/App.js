import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import InventoryForm from './components/InventoryForm';
import InventoryList from './components/InventoryList';
import ManufacturerForm from './components/ManufacturerForm';
import ManufacturerList from './components/ManufacturerList';
import ModelForm from './components/ModelForm';
import ModelList from './components/ModelList';
import ServiceAppointment from './components/ServiceAppointment';
import ServiceAppointmentForm from './components/ServiceAppointmentForm';
import ServiceTechnicianForm from './components/ServiceTechnicianForm';
import ServiceHistory from './components/ServiceHistory';
import SalesPersonForm from './components/SalesPersonForm';
import SalesCustomerForm from './components/SalesCustomerForm';
import SalesCustomerList from './components/SalesCustomerList';
import SalesRecordForm from './components/SalesRecordForm';
import SalesPersonHistory from './components/SalesPersonHistory';
import SalesList from './components/SalesList';
import CompanyInfo from './CompanyInfo';
import './App.css';

function App() {
  return (
    <div className='page-container'>
      <div className='content-wrap'>
        <BrowserRouter>
          <Nav />
          <div className="container">
            <Routes>
              <Route path="/" element={<MainPage />} />
              <Route path="/inventory/form" element={<InventoryForm />} />
              <Route path="/inventory/list" element={<InventoryList />} />
              <Route path="/manufacturer/form" element={<ManufacturerForm />} />
              <Route path="/manufacturer/list" element={<ManufacturerList />} />
              <Route path="/model/form" element={<ModelForm />} />
              <Route path="/model/list" element={<ModelList />} />
              <Route path="/service/list" element={<ServiceAppointment />} />
              <Route path="/service/new" element={<ServiceAppointmentForm />} />
              <Route path="/service/history" element={<ServiceHistory />} />
              <Route path="/service/technician/new" element={<ServiceTechnicianForm />} />
              <Route path="/sales/list" element={<SalesList />} />
              <Route path="/sales/new" element={<SalesRecordForm />} />
              <Route path="/sales/salesperson/new" element={<SalesPersonForm />} />
              <Route path="/sales/salesperson/history" element={<SalesPersonHistory />} />
              <Route path="/sales/customer/new" element={<SalesCustomerForm />} />
              <Route path="/sales/customer" element={<SalesCustomerList />} />
              <Route path="/companyinfo/" element={<CompanyInfo />} />
            </Routes>
          </div>
        </BrowserRouter>
      </div>
      <div className="navbar navbar-fixed-bottom">CopyRight @2023 Rigby & Grey</div>
    </div>
  );
}
export default App;
