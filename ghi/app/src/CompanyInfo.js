function CompanyInfo() {
    return (
        <div className="px-4 py-5 my-5 text-center text-black bg-light opacity-75">
          <h2 className="display-5 fw-bold">
            <p>Team:</p>
            <p>Jeremiah Rigby - Service</p>
            Dean Grey - Sales
</h2>

<h3>

<p>Design</p>
<br />
<p>Service</p>
<p>I only have two models I have the Technician Model which is for adding a new technician into the system, I also have an Appointment Model with a foreign key to the Technician Model,
this allows you to attach a technician to each appointment along with all the other information needed.

    My integration with the inventory microservice is for checking if any cars that have an appointment with the service department were ever in the inventory (ie: we sold them the car),
and if so to show in the service appts list that they were a VIP, I opted to alter there status directly in the poller rather than make a VO just to be more effiecient, so whenever it polls,
it will check if the vin was ever in the inventory and if so it will update the appointment instance that carries that vin and update the vip status and thus change the List to show that they
are indeed a VIP customer.</p>

<p>Sales</p>
<p>I have 4 models representing the Customer, Salesperson, SalesRecord and AutomobileVO which I used for my poller to detect changes in the inventory.  The SalesRecord has 3 foreign keys relating to the automobiles, customers, and salespeople.

I chose a filtered list by salesperson which shows each salesperson’s sales history but not a static list of all sales.  We also chose to add a sponsor modal, and an off canvas sidebar with pertinent link information.
</p>
</h3>

          <div className="col-lg-6 mx-auto">
            <div className="lead mb-4">
               
            </div>
          </div>
         </div>
    );
  }
  export default CompanyInfo;