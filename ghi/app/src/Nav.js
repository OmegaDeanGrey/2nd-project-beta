import { Link, NavLink } from 'react-router-dom';
import "./nav.css"

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-danger" id="thenav">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">Kai's Cars</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <NavLink className="nav-link active" aria-current="page" to="/">Home</NavLink>
            </li>

            <div className="dropdown mx-1 ">
              <button
                className="btn btn-danger text-light  dropdown-toggle"
                type="button"
                id="dropdownMenuButton1"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                Inventory
              </button>
              <ul
                className="dropdown-menu"
                aria-labelledby="dropdownMenuButton1"
              >
                <li>
                  <Link className="dropdown-item" to="/manufacturer/list">
                    Manufacturers
                  </Link>
                </li>
                <li>
                  <Link className="dropdown-item" to="/manufacturer/form">
                    Add Manufacturer
                  </Link>
                </li>
                <li>
                  <Link className="dropdown-item" to="/model/list">
                    Models
                  </Link>
                </li>
                <li>
                  <Link className="dropdown-item" to="/model/form">
                    Add Model
                  </Link>
                </li>
                <li>
                  <Link className="dropdown-item" to="/inventory/list">
                    Automobiles
                  </Link>
                </li>
                <li>
                  <Link className="dropdown-item" to="/inventory/form">
                    Add to Inventory
                  </Link>
                </li>
              </ul>
            </div>
            <div className="dropdown mx-1">
              <button
                className="btn btn-danger text-light  dropdown-toggle"
                type="button"
                id="dropdownMenuButton1"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                Appointments
              </button>
              <ul
                className="dropdown-menu"
                aria-labelledby="dropdownMenuButton1"
              >
                <li>
                  <Link className="dropdown-item" to="/service/list">
                    Service Appointments
                  </Link>
                </li>
                <li>
                  <Link className="dropdown-item" to="/service/new">
                    New Appointment
                  </Link>
                </li>
              </ul>
            </div>
            <div className="dropdown mx-1">
              <button
                className="btn btn-danger text-light  dropdown-toggle"
                type="button"
                id="dropdownMenuButton1"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                Service
              </button>
              <ul
                className="dropdown-menu"
                aria-labelledby="dropdownMenuButton1"
              >
                <li>
                  <Link className="dropdown-item" to="/service/history">
                    Service History
                  </Link>
                </li>
                <li>
                  <Link className="dropdown-item" to="/service/technician/new">
                    Add Technician
                  </Link>
                </li>
              </ul>
            </div>
            <div className="dropdown mx-1">
              <button
                className="btn btn-danger text-light  dropdown-toggle"
                type="button"
                id="dropdownMenuButton1"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                Sales
              </button>
              <ul
                className="dropdown-menu"
                aria-labelledby="dropdownMenuButton1"
              >
                <li>
                  <Link className="dropdown-item" to="/sales/list">
                    Sales List
                  </Link>
                </li>
                <li>
                  <Link className="dropdown-item" to="/sales/new">
                    Add Sale
                  </Link>
                </li>
              </ul>
            </div>
            <div className="dropdown mx-1">
              <button
                className="btn btn-danger text-light  dropdown-toggle"
                type="button"
                id="dropdownMenuButton1"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                Customers
              </button>
              <ul
                className="dropdown-menu"
                aria-labelledby="dropdownMenuButton1"
              >
                <li>
                  <Link className="dropdown-item" to="/sales/customer">
                    Customer List
                  </Link>
                </li>
                <li>
                  <Link className="dropdown-item" to="/sales/customer/new">
                    Add Customer
                  </Link>
                </li>
              </ul>
            </div>
            <div className="dropdown mx-1">
              <button
                className="btn btn-danger text-light  dropdown-toggle"
                type="button"
                id="dropdownMenuButton1"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                SalesPeople
              </button>
              <ul
                className="dropdown-menu"
                aria-labelledby="dropdownMenuButton1"
              >
                <li>
                  <Link className="dropdown-item" to="/sales/salesperson/history">
                    SalesPerson List
                  </Link>
                </li>
                <li>
                  <Link className="dropdown-item" to="/sales/salesperson/new">
                    Add Salesperson
                  </Link>
                </li>
              </ul>
            </div>
          </ul>
          <NavLink className="navbar-brand text-danger" to="/companyinfo/">
            Company Info
          </NavLink>
        </div>
      </div>
    </nav>
  )
}
export default Nav;
