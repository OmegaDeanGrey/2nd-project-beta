import { useState, useEffect } from "react";


function InventoryForm() {
  const noData = {
    color: "",
    year: "",
    vin: "",
  }
  const [inventoryData, setInventoryData] = useState(noData)
  const [model, setModel] = useState()
  const [modelData, setModelData] = useState([])

  const handleChange = (event) => {
    setInventoryData({ ...inventoryData, [event.target.name]: event.target.value })
  }

  const handleModelChange = (event) => {
    setModel(event.target.value)
  }

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {}
    data.color = inventoryData.color
    data.year = inventoryData.year
    data.vin = inventoryData.vin
    data.model_id = parseInt(model)
    const inventoryUrl = "http://localhost:8100/api/automobiles/";
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(data),
      headers: { "Content-Type": "application.json" }
    }
    const response = await fetch(inventoryUrl, fetchConfig);
    if (response.ok) {
      const newInventory = await response.json();
      setInventoryData(noData);
      alert(`Welcome, ${newInventory.vin}!`);
    } else {
      alert("Something went wrong");
    }
  }

  const fetchData = async () => {
    const response = await fetch("http://localhost:8100/api/models/")
    const modelData = await response.json()
    const models = (modelData.models)
    setModelData(models)
  }

  useEffect(() => {
    fetchData()
  }, []
  )


  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1 className="text-white">Add New Automobile</h1>
          <form onSubmit={handleSubmit} id="inventory-form">
            <div className="form-floating mb-3">
              <input onChange={handleChange} value={inventoryData.color} placeholder="Enter Vehicle Color" required type="text" name="color" id="color" className="form-control" />
              <label htmlFor="color">Color</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleChange} value={inventoryData.year} placeholder="Enter Vehicle Year " required type="text" name="year" id="year" className="form-control" />
              <label htmlFor="year">Year</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleChange} value={inventoryData.vin} placeholder="Enter Vehicle VIN" required type="text" name="vin" id="vin" className="form-control" />
              <label htmlFor="vin">VIN</label>
            </div>
            <div className="mb-3">
              <select onChange={handleModelChange} required name="model" id="model" className="form-select" value={model}>
                <option value="">Model</option>
                {modelData.map(model => {
                  return (
                    <option key={model.id} value={parseInt(model.id)}>
                      {model.name}
                    </option>
                  )
                })}
              </select>
            </div>
             <button className="btn btn-light">Create</button>
          </form>
        </div>
      </div>
    </div>
  )
}
export default InventoryForm;
