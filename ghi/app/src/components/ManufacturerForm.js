import { useState } from "react";


function ManufacturerForm() {
    const noData = {
        name: "",
    }


    const [manufacturerData, setManufacturerData] = useState(noData)
    const handleChange = (event) => {
        setManufacturerData({...manufacturerData, [event.target.name]: event.target.value});
    }


    const handleSubmit = async (event) => {
        event.preventDefault();
        const manufacturerUrl = "http://localhost:8100/api/manufacturers/";
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify({...manufacturerData}),
            headers: {"Content-Type": "application.json"}
        }
        const response = await fetch(manufacturerUrl, fetchConfig);
        if (response.ok) {
            const newManufacturer = await response.json();
            setManufacturerData(noData);
            alert(`Welcome, ${newManufacturer.name}!`);
        } else {
            alert("Something went wrong");
        }
    }


    return(
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1 className="text-white">Add Manufacturer</h1>
            <form onSubmit={handleSubmit} id="manufacturer-form">
              <div className="form-floating mb-3">
                <input onChange={handleChange} value={manufacturerData.name} placeholder="Enter Manufacturer Name" required type="text" name="name" id="name" className="form-control" />
                <label htmlFor="name">Manufacturer Name</label>
              </div>
              <button className="btn btn-danger">Create</button>
            </form>
          </div>
        </div>
        </div>
    )
}
export default ManufacturerForm;