import React, { useEffect, useState } from "react";

function ModelList(){
    const [models, setModel] = useState([])

    const getModelData = async () => {
        const response = await fetch("http://localhost:8100/api/models/")
        const data = await response.json()
        setModel(data.models)
    }

    useEffect(()=>{
        getModelData()
    },[]
    )

    return(

        <div className="text-white">
            <h1>
                Models In Inventory
            </h1>
            <table className="table table-muted table-hover table-striped text-white">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Manufacturer</th>
                        <th>Picture</th>
                    </tr>
                </thead>
                <tbody className="text-white">
                    {models?.map(model=> {
                    return(
                    <tr key={model.id}>
                        <td className="text-white"> { model.name }</td>
                        <td className="text-white"> {model.manufacturer.name }</td>
                        <td>
                        <td className="d-flex justify-contend-md-center"><img src={model.picture_url} alt="" width="150px" height="1000" className="img-fluid img-thumbnail" /></td>
                        </td>
                    </tr>
                    ) })}
                </tbody>
            </table>
        </div>

    )
}
export default ModelList;






                    
