import React, { useState, useEffect } from 'react'
import ServiceVip from './ServiceVip'


export default function ServiceAppointment() {
    const [appointments, setAppointments] = useState([])


    function notCompleted(appointment) {
        return appointment.appointment_status === false
    }


    const fetchData = async () => {
        const response = await fetch("http://localhost:8080/api/appointments/")
        const appointmentData = await response.json()
        const showedAppts = (appointmentData.appointments).filter(notCompleted)
        setAppointments(showedAppts)
    }


    const handleDelete = async (id) => {
        const url = `http://localhost:8080/api/appointments/${id}`
        const fetchConfig = {
            method: "DELETE"
        }
        const response = await fetch(url, fetchConfig)
        if (response.ok) {
            alert("Appt Deleted")
            fetchData()
        }
        else {
            alert("No worky")
        }

    }


    const handleComplete = async (id) => {
        const url = `http://localhost:8080/api/appointments/${id}/`
        const data = { appointment_status: true }
        const fetchConfig = {
            method: "PUT",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        }
        await fetch(url, fetchConfig)
        fetchData()
    }


    useEffect(() => {
        fetchData()
    }, [])


    return (
        <div>
            <h1 className='text-light'>Service Appointments</h1>
            <table className='table table-danger'>
                <thead>
                    <tr>
                        <th>Vin</th>
                        <th>Customer Name</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Reason For Visit</th>
                        <th>VIP</th>
                        <th>Technician</th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    {appointments.map(appointment => (
                        <tr key={appointment.vin}>
                            <td>{appointment.vin}</td>
                            <td>{appointment.customer_name}</td>
                            <td>{appointment.date}</td>
                            <td>{appointment.time}</td>
                            <td>{appointment.reason}</td>
                            <td>{<ServiceVip vip={appointment.vip} />}</td>
                            <td>{appointment.technician.name}</td>
                            <td><button onClick={() => handleComplete(appointment.id)}>Finish</button></td>
                            <td><button onClick={() => handleDelete(appointment.id)}>Delete</button></td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    )
}
