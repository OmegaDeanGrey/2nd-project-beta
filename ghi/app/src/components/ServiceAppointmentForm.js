import React, { useState, useEffect } from 'react'

export default function ServiceAppointmentForm() {
    const [technicians, setTechnicians] = useState([])
    const [vin, setVin] = useState("")
    const [customerName, setCustomerName] = useState("")
    const [date, setDate] = useState("")
    const [time, setTime] = useState("")
    const [reason, setReason] = useState("")
    const [technician, setTechnician] = useState("")


    const handleVinChange = (e) => {
        const value = e.target.value
        setVin(value)
    }


    const handleCustomerNameChange = (e) => {
        const value = e.target.value
        setCustomerName(value)
    }


    const handleDateChange = (e) => {
        const value = e.target.value
        setDate(value)
    }


    const handleTimeChange = (e) => {
        const value = e.target.value
        setTime(value)
    }


    const handleReasonChange = (e) => {
        const value = e.target.value
        setReason(value)
    }


    const handleTechnicianChange = (e) => {
        const value = e.target.value
        setTechnician(value)
    }


    const handleSubmit = async (e) => {
        e.preventDefault()
        const data = {}
        data.vin = vin
        data.customer_name = customerName
        data.date = date
        data.time = time
        data.reason = reason
        data.technician = technician


        const apptUrl = "http://localhost:8080/api/appointments/"
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        }
        const apptResponse = await fetch(apptUrl, fetchConfig)
        if (apptResponse.ok) {
            setVin("")
            setCustomerName("")
            setTime("")
            setDate("")
            setReason("")
            setTechnician("")
        }
    }


    const fetchData = async () => {
        const response = await fetch("http://localhost:8080/api/technicians/")
        const technicianData = await response.json()
        setTechnicians(technicianData.technicians)
    }


    useEffect(() => {
        fetchData()
    }, [])


    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4 bg-danger">
                    <h1 className="text-white">Add New Appointment</h1>
                    <form onSubmit={handleSubmit} id="appointment-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleVinChange} value={vin} placeholder="Enter your vin" required type="text" name="vin" id="vin" className="form-control" maxLength={17} />
                            <label htmlFor="vin">Vin</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleCustomerNameChange} value={customerName} placeholder="Enter customer name" required type="text" name="customer_name" id="customer_name" className="form-control" />
                            <label htmlFor="customer_name">Customer Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleDateChange} value={date} placeholder="Enter date" required type="date" name="date" id="date" className="form-control" />
                            <label htmlFor="date">Date</label>
                        </div>

                        <div className="form-floating mb-3">
                            <input onChange={handleTimeChange} value={time} placeholder="Enter time" required type="time" name="time" id="time" className="form-control" />
                            <label htmlFor="time">Time</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleReasonChange} value={reason} placeholder="Reason for visit" required type="text" name="reason" id="reason" className="form-control" />
                            <label htmlFor="reason">Reason for Visit</label>
                        </div>
                        <div className=" mb-3">
                            <select onChange={handleTechnicianChange} value={technician} placeholder="Technician" name="technician" id="technician" className="form-control" required>
                                <option key={technician.name} value="">Assign a technician</option>
                                {technicians.map(technician => {
                                    return (
                                        <option key={technician.employee_id} value={technician.name}>{technician.name}</option>
                                    )
                                })}
                            </select>
                        </div>
                        <button className="btn btn-light">Add Appointment</button>
                    </form>
                </div>
            </div>
        </div>
    )
}
