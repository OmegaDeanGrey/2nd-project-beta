import React, { useState, useEffect } from 'react'

export default function ServiceHistory() {
    const [appointments, setAppointments] = useState([])
    const [filterVin, setFilterVin] = useState("")


    const handleVinInputChange = (e) => {
        setFilterVin(e.target.value)
        fetchData()
    }


    const vinFilter = (appt) => {
        return appt.vin.includes(filterVin)
    }


    const fetchData = async () => {
        const response = await fetch("http://localhost:8080/api/appointments/")
        const appointmentData = await response.json()
        const appts = (appointmentData.appointments)
        const filtered = await appts.filter(vinFilter)
        setAppointments([...filtered])
    }


    useEffect(() => {
        fetchData()
    }, [])


    useEffect(() => {
        fetchData()
    }, appointments)


    return (
        <div>
            <h1 className='text-light'>Service Appointments</h1>
            <div className="form-floating mb-3">
                <input onChange={handleVinInputChange} value={filterVin} placeholder="Enter vin" required type="text" name="vin" id="vin" className="form-control" maxLength={17} />
                <label htmlFor="vin">Vin</label>
            </div>
            <table className='table table-danger'>
                <thead>
                    <tr>
                        <th>Vin</th>
                        <th>Customer Name</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Reason For Visit</th>
                        <th>Technician</th>
                    </tr>
                </thead>
                <tbody>
                    {appointments.map(appointment => (
                        <tr key={appointment.vin}>
                            <td>{appointment.vin}</td>
                            <td>{appointment.customer_name}</td>
                            <td>{appointment.date}</td>
                            <td>{appointment.time}</td>
                            <td>{appointment.reason}</td>
                            <td>{appointment.technician.name}</td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>


    )
}
