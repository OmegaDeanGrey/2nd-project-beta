from django.contrib import admin
from .models import Customer, SalesPerson, AutomobileVO, SalesForm

@admin.register(Customer)
class Customer(admin.ModelAdmin):
    pass

@admin.register(SalesPerson)
class SalesPerson(admin.ModelAdmin):
    pass

@admin.register(AutomobileVO)
class AutomobileVO(admin.ModelAdmin):
    pass

@admin.register(SalesForm)
class SalesForm(admin.ModelAdmin):
    pass