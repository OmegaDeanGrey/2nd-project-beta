from django.db import models


class Customer(models.Model):
    name = models.CharField(max_length = 100)
    address = models.CharField(max_length = 200)
    phone_number = models.CharField(max_length = 14)

    def __str__(self):
        return f"{self.name}"

class SalesPerson(models.Model):
    name = models.CharField(max_length = 100)
    employee_number = models.PositiveSmallIntegerField(default = 1, unique = True)

    def __str__(self):
        return f"{self.name}"


class AutomobileVO(models.Model):
    vin = models.CharField(max_length = 17)

    def __str__(self):
        return f"{self.vin}"


class SalesForm(models.Model):
    price = models.CharField(max_length = 20)
    customer = models.ForeignKey(
        Customer,
        related_name="customer",
        on_delete=models.CASCADE, 
        null=True, blank=True,
    )
    salesperson = models.ForeignKey(
        SalesPerson,
        related_name="salesperson",
        on_delete=models.CASCADE, 
        null=True, blank=True,
    )
    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="salesform",
        on_delete=models.CASCADE, 
        null=True, blank=True,
    )