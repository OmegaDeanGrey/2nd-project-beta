from django.urls import path
from .views import api_customer, api_customers, api_salesperson, api_salespersons, api_saleslist, api_salesform

urlpatterns = [
    path("customer/<int:id>", api_customer, name="api_salesForm"),
    path("customers/", api_customers, name="api_salesList"),
    path("salesperson/<int:id>/", api_salesperson, name="api_salesperson"),
    path("salespersons/", api_salespersons, name="api_salespersons"),
    path("salesforms/", api_saleslist, name="api_customers"),
    path("salesform/<int:id>/", api_salesform, name="api_customer"),
]
